# AutoDibs #

This is a python project to automate the reservation process for the FGCU library system.

It is meant to be used from the commandline and requires the Selenium python package from [PyPi](https://pypi.python.org/pypi/selenium).

Usage

```
#!shell
reserve.py [-h] [-l length] [-d days] [-t "time"] "room"

```

Example:

```
#!shell

 reserve.py -l 1.0 -d 7 -t "00:14:00" "123 East"
```


This reserves room 123 East for 1 hour 7 days from today at 2 P.M.