import argparse
import sys
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import os
from urllib.parse import urlparse, parse_qs, urlencode
from datetime import datetime, timedelta

driver = webdriver.Chrome(
    os.getcwd() + "/drivers/" + sys.platform + "/webdriver" + ".exe" if sys.platform == "win32" else "")
reserve_info = "Juan" + Keys.TAB \
               + "Menendez" + Keys.TAB \
               + "jjmenendez3730@eagle.fgcu.edu" + Keys.TAB \
               + "3055104001" + Keys.TAB + Keys.RETURN


def reserve():
    args = getArgs()
    dt = (datetime.today() + timedelta(days=args.day))
    timeParams = {
        "languageCodeChange": "en-US",
        "SelectedTime": formatLength(args.length),
        "SelectedTimeSort": "AnyTime",
        "SelectedSearchDate": dt.strftime("%Y/%m/%d"),
        "SelectedRoomID": getSpaceNumber(args.room),
        "SelectedBuildingID": 0,
        "RoomIDPassedIn": "True",
        "SelectedStartTime": dt.strftime("%Y/%m/%d ") + args.time
    }
    print("Submitting reservation for room {} for {} hour(s) {} days from today at {}"
          .format(args.room, args.length, args.day, args.time))
    driver.get("http://fgcu.evanced.info/Dibs/Registration?" + urlencode(timeParams))
    driver.find_element_by_id("FirstName").send_keys(reserve_info)
    print("If the reservation was successful, you'll get a confirmation text shortly")


def getArgs():
    parser = argparse.ArgumentParser(description="Reserves a room at the FGCU library using Dibs system.",
                                     usage="reserve.py [-h] [-l length] [-d days] [-t \"time\"] \"room\"\n" +
                                           "Example: reserve.py -l 1.0 -d 7 -t \"00:14:00\" \"123 East\"\n" +
                                           "This reserves room 123 East for 1 hour 7 days from today at 2 P.M.")
    parser.add_argument("room",
                        type=str,
                        help="Room to reserve. Ex 129 East")
    parser.add_argument("-l", "--length",
                        type=float,
                        help="Length of time to reserve room",
                        nargs="?",
                        choices=[0.5, 1.0, 1.5, 2.0],
                        default=2.0)
    parser.add_argument("-d", "--day",
                        type=int,
                        help="Number of days ahead to schedule",
                        nargs="?",
                        choices=list(range(8)),
                        default=7)
    parser.add_argument("-t", "--time",
                        type=str,
                        help="Time of day to start reservation",
                        nargs="?",
                        default="07:00:00")
    return parser.parse_args()


def getSpaceNumber(rm):
    driver.get("http://library.fgcu.edu/csd/dibsfav.html")
    room_ele = driver.find_element_by_link_text(rm)
    print("Found room -> " + room_ele.get_attribute("href"))
    return room_ele.get_attribute("href").split("=")[1]


def formatLength(n):
    return n if n % 1.0 != 0.0 else int(n)


if __name__ == "__main__":
    reserve()
    driver.quit()
